(function() {
  var publicAPI = {};

  var findNode = function(data, ancestors, nodeAttrs) {
    var i,
      currentChild,
      result;

    if (nodeAttrs.val === data[nodeAttrs.key]) {
      return data;
    } else {

      // Use a for loop instead of forEach to avoid nested functions
      // Otherwise "return" will not work properly
      for (i = 0; data[nodeAttrs.childKey] !== undefined && i < data[nodeAttrs.childKey].length; i += 1) {
        currentChild = data[nodeAttrs.childKey][i];

        // Search in the current child
        result = findNode(currentChild, ancestors, nodeAttrs);

        // Return the result if the node has been found
        if (result !== false) {
          ancestors.push(data[nodeAttrs.key]);
          result.ancestors = ancestors;
          return result;
        }
      }

      // The node has not been found and we have no more options
      return false;
    }
  }

  publicAPI.findNode = findNode;
  window.utility = publicAPI;
  
})();
