(function() {

  let publicAPI = {};

  let createTree = function(obj, attrs) {
    let treeNode = null;

    if (obj[attrs.key]) {
      // If it's an object, create the <li> and <a>
      let anchorNode = document.createElement('a');
      let textNode = document.createTextNode(obj[attrs.key]);
      anchorNode.appendChild(textNode);

      let attr = document.createAttribute('href');
      attr.value = '#';
      anchorNode.setAttributeNode(attr);

      treeNode = document.createElement('li');
      treeNode.appendChild(anchorNode);

      // if there are any children, append them recursively
      if (obj[attrs.childKey]) {
        treeNode.appendChild(createTree(obj[attrs.childKey], attrs));
      }
    } else if (obj.length) {
      // If it's an array with some elements, create the <ul>
      treeNode = document.createElement("ul");

      for (var i = 0, l = obj.length; i < l; i++) {
        treeNode.appendChild(createTree(obj[i], attrs));
      }
    }

    // if it was an empty array or an object that doesn't have a title
    // property, then it'll just return null;
    return treeNode;
  }

  publicAPI.createTree = createTree;
  window.utility = publicAPI;

})();
